<?php

namespace Tests\Unit;

use Azizyus\LaravelLanguageHelper\App\Events\TranslationSaved;
use Azizyus\LaravelLanguageHelper\App\LanguageHelperServiceProvider;
use Azizyus\LaravelLanguageHelper\App\Models\Language;
use Azizyus\LaravelLanguageHelper\App\Models\TestDummyModel;
use Azizyus\LaravelLanguageHelper\App\Models\TestModel;
use Azizyus\LaravelLanguageHelper\App\Models\Translation;
use Azizyus\LaravelLanguageHelper\App\Slug\SlugHelper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Str;
use Mockery\Mock;
use Tests\TestCase;

class SlugTest extends TestCase
{

    protected function getPackageProviders($app)
    {
        return [LanguageHelperServiceProvider::class];
    }

    protected function setUp(): void
    {
        parent::setUp();
        $this->loadLaravelMigrations();
        $this->loadMigrationsFrom(__DIR__ . '/../../src/TestMigrations');
        $this->artisan('migrate:refresh')->run();
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testBasicTest()
    {


        $input = [
            'title1' => 'TEST SLUG 1'
        ];
        $r = new Request($input);
//
        $language =  Language::create([
            'title' => 'turkish',
            'shortTitle' => 'TR'
        ]);

        $testModel = TestModel::create();
        $testModel->updateAllTranslations(collect([$language]),$r);

        $first = Translation::where('property','slug')->first();


        if(SlugHelper::slug($input["title$language->id"]) == $first->data)
            $this->assertTrue(true);
        else $this->assertTrue(false);

    }


    /**
     * Define environment setup.
     *
     * @param  \Illuminate\Foundation\Application  $app
     * @return void
     */
    protected function getEnvironmentSetUp($app)
    {
        $app['config']->set('database.default', 'mysql');
    }


}
