<?php


namespace Azizyus\LaravelLanguageHelper\App\Slug;


use Illuminate\Support\Str;

class SlugHelper
{
    public static function slug($slug=null)
    {
        return Str::slug($slug);
    }
}
