<?php

namespace Azizyus\LaravelLanguageHelper\App\Commands;

use Azizyus\LaravelLanguageHelper\App\Commands\Helpers\CheckLanguageExist;
use Azizyus\LaravelLanguageHelper\App\Models\Language;
use Azizyus\LaravelLanguageHelper\App\Models\LanguageWithoutDeleted;
use Azizyus\LaravelLanguageHelper\App\Repositories\Eloquent\LanguageRepository;
use Illuminate\Console\Command;

class UpdateLanguage extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'language:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'update your language via cli';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {


        while (true)
        {

            foreach (LanguageWithoutDeleted::all() as $lang)
            {
                $this->comment("[$lang->id] $lang->title ($lang->shortTitle)");
            }


            $id = $this->ask("choose your language to update by id");



            $language = LanguageWithoutDeleted::find($id);

            if($language)
            {
                $this->comment("i found your lang");
                break;
            }
            else
            {
                $this->comment("probably you wrote wrong id");
            }
        }



        $columns = $language->toArray();


       while (true)
       {


           foreach ($columns as $key => $data)
           {
               $this->comment("$key => $data");
           }
           $key = $this->ask("choose key to update (enter # for save)");


           if($key == "#")
           {
               break;
           }

           if(!in_array($key,array_keys($columns)))
           {
                $this->comment("cant find your key");
                continue;
           }


           $newData = $this->ask("new value?");


           $columns[$key] = $newData;
           $this->comment("here s updated version of columns");








       }


        $language->update($columns);
        $language->save();

        $this->comment("saved");





    }
}
