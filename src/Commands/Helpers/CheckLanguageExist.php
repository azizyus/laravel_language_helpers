<?php
/**
 * Created by PhpStorm.
 * User: dd
 * Date: 21.01.2019
 * Time: 11:35
 */

namespace Azizyus\LaravelLanguageHelper\App\Commands\Helpers;

use Azizyus\LaravelLanguageHelper\App\Models\Language;
use Illuminate\Database\Eloquent\Builder;

class CheckLanguageExist
{

    public static function isExist(array $array,Builder $language) : Bool
    {

        foreach ($array as $key => $item)
        {
            $language->orWhere($key,$item);
        }
        if($language->first())
        {
            return true;
        }
        else
        {
           return false;
        }

    }


}