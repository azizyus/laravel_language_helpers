<?php

namespace Azizyus\LaravelLanguageHelper\App\Commands;

use Azizyus\LaravelLanguageHelper\App\Commands\Helpers\CheckLanguageExist;
use Azizyus\LaravelLanguageHelper\App\Models\Language;
use Azizyus\LaravelLanguageHelper\App\Models\LanguageWithoutDeleted;
use Azizyus\LaravelLanguageHelper\App\Repositories\Eloquent\LanguageRepository;
use Illuminate\Console\Command;

class DeleteLanguage extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'language:delete';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'add your language via cli';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {


        while (true)
        {
            $array = [
                "shortTitle" => $this->ask("shortTitle?"),
            ];

            $language = LanguageWithoutDeleted::query();

            if(CheckLanguageExist::isExist($array,$language))
            {
                $this->comment("i found your lang");
                break;

            }
            else
            {
                $this->comment("cant find the language you are trying to find");
            }


        }

        LanguageWithoutDeleted::where("shortTitle",$array["shortTitle"])->delete();
        $this->comment("your language deleted (soft)");



    }
}
