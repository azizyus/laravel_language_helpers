<?php
/**
 * Created by PhpStorm.
 * User: azizyus
 * Date: 3/15/19
 * Time: 1:07 AM
 */

namespace Azizyus\LaravelLanguageHelper\App\Factory;


use Azizyus\LaravelLanguageHelper\App\Models\ILanguage;
use Azizyus\LaravelLanguageHelper\App\Models\LanguageWithoutDeleted;
use Azizyus\LaravelLanguageHelper\App\SortHelper\SortHelper;

class Factory
{


    public function make() : ILanguage
    {
        $language = new LanguageWithoutDeleted();
        $language->isActive = true;
        $language->sort = SortHelper::getLatestSort(LanguageWithoutDeleted::class);
        return $language;
    }

}