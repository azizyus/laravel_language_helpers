<?php
/**
 * Created by PhpStorm.
 * User: dd
 * Date: 22.07.2018
 * Time: 22:54
 */

namespace Azizyus\LaravelLanguageHelper\App\SortHelper;


class SortHelper
{

    public static function getLatestSort($model,$column="sort")
    {


        $result = $model::orderBy($column,"DESC")->first();


        if($result) return ($result->sort+1);
        else return 0;

    }

}