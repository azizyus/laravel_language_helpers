<?php
/**
 * Created by PhpStorm.
 * User: dd
 * Date: 02.12.2018
 * Time: 18:31
 */

namespace Azizyus\LaravelLanguageHelper\App;

use Azizyus\LaravelLanguageHelper\App\Commands\AddLanguage;
use Azizyus\LaravelLanguageHelper\App\Commands\DeleteLanguage;
use Azizyus\LaravelLanguageHelper\App\Commands\ListLanguages;
use Azizyus\LaravelLanguageHelper\App\Commands\UpdateLanguage;
use Illuminate\Support\ServiceProvider;

class LanguageHelperServiceProvider extends ServiceProvider
{


    public function boot()
    {


        $this->commands([
            AddLanguage::class,
            DeleteLanguage::class,
            ListLanguages::class,
            UpdateLanguage::class,
        ]);


        $this->publishes([


            __DIR__."/Migrations"=>database_path("migrations"),


        ],"azizyus/laravel_language_helpers-publish-migrations");

        $this->publishes([


            __DIR__."/Config/language-config.php"=>config_path("language-config.php"),


        ],"azizyus/laravel_language_helpers-publish-config");




        $this->mergeConfigFrom(__DIR__."/Config/current-language.php","current-language");
        $this->mergeConfigFrom(__DIR__."/Config/language-config.php","language-config");

        $this->loadMigrationsFrom(__DIR__."/Migrations");

    }

    public function register()
    {

    }

}