<?php
/**
 * Created by PhpStorm.
 * User: dd
 * Date: 23.07.2018
 * Time: 09:46
 */

namespace Azizyus\LaravelLanguageHelper\App\Repositories\Eloquent;


use Azizyus\LaravelLanguageHelper\App\Factory\Factory;
use Azizyus\LaravelLanguageHelper\App\Models\Language;
use Azizyus\LaravelLanguageHelper\App\Models\LanguageWithoutDeleted;
use Azizyus\LaravelLanguageHelper\App\SortHelper\SortHelper;
use Azizyus\LaravelBasicUploadHelper\UploadHelper;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class LanguageRepository
{

    protected $factory;
    public function __construct()
    {
        $this->factory = new Factory();
    }

    public function setFactory(Factory $factory)
    {
        $this->factory = $factory;
    }

    public function updateOrInsert(Request $request,$id=null)
    {


        $language = $this->getFirst($id);


        if($language==null)
        {
            $language = $this->factory->make();
        }

        if($request->exists("removeImage"))
        {
            UploadHelper::removeAllImageVersions($language,"languageImage");
            $language->languageImage = null;
        }

        $language->languageImage = UploadHelper::catchSingleImageAndPutModelPropertyThenDeleteOldImage($language,"languageImage",$request,"languageImage");
        $language->title = $request->get("title");
        $language->shortTitle = $request->get("shortTitle");
        $language->locale = $request->get("locale");



        $language->save();


    }


    public function getByShortTitle(string $shortTitle=null)
    {
        return $this->baseQuery()->where("shortTitle",$shortTitle)->first();
    }

    protected function baseQuery() : Builder
    {
        return LanguageWithoutDeleted::query();
    }

    public function getFirst($id)
    {
        return $this->baseQuery()->where("id",$id)->first();
    }

    public function firstOrNew($id,$instance)
    {
        $result = $this->baseQuery()->where("id",$id)->first();

        if(!$result) return $instance;

        return $result;
    }

    public function getAll()
    {
        return $this->baseQuery()->get();
    }

    public function delete($id)
    {


        $language = $this->getFirst($id);

        $language->delete();

        return true;

    }

}