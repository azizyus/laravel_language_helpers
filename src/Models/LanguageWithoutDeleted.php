<?php

namespace Azizyus\LaravelLanguageHelper\App\Models;

use Azizyus\LaravelBasicUploadHelper\UploadHelper;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property int $id
 * @property string $title
 * @property string $shortTitle
 * @property boolean $isActive
 * @property boolean $isDefault
 * @property int $sort
 * @property string $created_at
 * @property string $updated_at
 * @property string $deleted_at
 */
class LanguageWithoutDeleted extends Language
{

    use SoftDeletes;

    protected $table="languages";

    public function hasImage()
    {
        if($this->languageImage==null) return false;

        return true;
    }


    public function getImage()
    {

        return UploadHelper::originalImagePrefixer($this->languageImage,true);

    }

    public function getImageFullPath()
    {
        return asset($this->getImage());
    }


    public function getShortTitle()
    {
        return strtolower($this->shortTitle);
    }
}
