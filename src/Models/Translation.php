<?php

namespace Azizyus\LaravelLanguageHelper\App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $property
 * @property string $data
 * @property int $languageId
 * @property int $tableEnum
 * @property int $modelId
 * @property string $created_at
 * @property string $updated_at
 * @property Language $language
 */
class Translation extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['property', 'data', 'languageId', 'tableEnum', 'modelId', 'created_at', 'updated_at'];

    /**
     * The storage format of the model's date columns.
     * 
     * @var string
     */
    protected $dateFormat = 'Y-m-d H:i:s';

    /**
     * The connection name for the model.
     * 
     * @var string
     */
    protected $connection = 'mysql';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function language()
    {
        return $this->belongsTo('Azizyus\LaravelLanguageHelper\App\Models\Language', 'languageId');
    }
}
