<?php


namespace Azizyus\LaravelLanguageHelper\App\Models;


use Azizyus\LaravelLanguageHelper\App\Models\Traits\HasLanguageProperties;
use Illuminate\Database\Eloquent\Model;

class TestModel extends Model
{

    protected $table = 'test_models';

    use HasLanguageProperties;

    public function getLanguageProperties(): array
    {
        return [
            'title',
            'slug'
        ];
    }

    public function translateEnum()
    {
        return 1;
    }

}
