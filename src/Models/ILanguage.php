<?php
/**
 * Created by PhpStorm.
 * User: azizyus
 * Date: 3/15/19
 * Time: 12:58 AM
 */

namespace Azizyus\LaravelLanguageHelper\App\Models;


interface ILanguage
{

    public function getId();
    public function getTitle();
    public function getShortTitle();

}