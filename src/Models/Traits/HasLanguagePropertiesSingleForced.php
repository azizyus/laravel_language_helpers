<?php
/**
 * Created by PhpStorm.
 * User: dd
 * Date: 14.11.2018
 * Time: 15:54
 */

namespace Azizyus\LaravelLanguageHelper\App\Models\Traits;


use Azizyus\LaravelLanguageHelper\App\Models\ILanguage;
use Azizyus\LaravelLanguageHelper\App\Models\Language;
use Azizyus\LaravelLanguageHelper\App\Models\Translated\DummyTranslated;
use Azizyus\LaravelLanguageHelper\App\Models\Translation;

trait HasLanguagePropertiesSingleForced
{


  use HasLanguageProperties;

  public function translate()
  {
      return $this->hasMany(config("language-config.mainModel"),"modelId",$this->primaryKey)
          ->where("languageId",config("current-language.languageId"))
          ->where("tableEnum",$this->translateEnum());
  }

}