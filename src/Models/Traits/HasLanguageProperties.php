<?php
/**
 * Created by PhpStorm.
 * User: dd
 * Date: 14.11.2018
 * Time: 15:54
 */

namespace Azizyus\LaravelLanguageHelper\App\Models\Traits;


use Azizyus\LaravelLanguageHelper\App\Models\ILanguage;
use Azizyus\LaravelLanguageHelper\App\Models\IMultipleLanguageObject;
use Azizyus\LaravelLanguageHelper\App\Models\Language;
use Azizyus\LaravelLanguageHelper\App\Models\Translated\DummyTranslated;
use Azizyus\LaravelLanguageHelper\App\Models\Translation;
use Azizyus\LaravelLanguageHelper\App\Slug\SlugHelper;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;


trait HasLanguageProperties
{


    /**
     * you must override to provide properties like a
     * [
     *  "title",
     * "description"
     * ]
     *
     */
    public function getLanguageProperties() : array
    {
        throw new \Exception("YOU DIDNT SET LANGUAGE PROPERTIES");
    }

    /**
     * you must override
     */
    public function translateEnum()
    {
        throw new \Exception(" YOU DIDNT SET TRANSLATE ENUM TO DEFINE YOUR TABLE IN QUERIES");
    }


    public function translate()
    {
        if(config("current-language.languageId") === null)
            return $this->hasMany(config("language-config.mainModel"),"modelId",$this->primaryKey)->where("tableEnum",$this->translateEnum());
        else
        {
            return $this->hasMany(config("language-config.mainModel"),"modelId",$this->primaryKey)
                ->where("languageId",config("current-language.languageId"))
                ->where("tableEnum",$this->translateEnum());
        }
    }



    public function getTranslated(ILanguage $language)
    {

        $translates = $this->translate;
        return $this->getTranslatedFromCollectionByLanguage($language,$translates);


    }

    public function updateTranslation(ILanguage $language,$properties=[])
    {

        $translations = $this->translate;

        foreach ($properties as $key => $property)
        {

            $foundProperty = $translations->where("languageId",$language->getId())->where("property",$key)->first();

            if(!$foundProperty)
            {
                $namespace = config("language-config.mainModel");
                $foundProperty = new $namespace();
                $foundProperty->property = $key;
                $foundProperty->languageId = $language->getId();
                $foundProperty->tableEnum = $this->translateEnum();
                $foundProperty->modelId = $this->id;
            }


            $foundProperty->data = $property;
            $foundProperty->save();


        }
        //lets say you have caching plugin which is probably will work with these events, so basically i need to fire these
        //because these language columns is logically part of your model
        $this->updateAndSaveEvents();


    }

    public function updateAllTranslations(Collection $languages,Request $request)
    {
        $languageProperties = $this->getLanguageProperties();
        foreach ($languages as $language)
        {
            foreach ($languageProperties as $languageProperty) {
                if ($request->has("$languageProperty" . "$language->id")) {
                    $this->updateTranslation($language, [
                        $languageProperty => $request->get("$languageProperty" . "$language->id"),
                    ]);
                }
            }

            if(in_array('slug',$languageProperties) && in_array('title',$languageProperties))
            {
                $this->updateTranslation($language,[
                    'slug' => SlugHelper::slug($request->get('title'.$language->id))
                ]);
            }

        }
    }

    public function updateAndSaveEvents()
    {
        static::fireModelEvent("saved");
        static::fireModelEvent("updated");
    }

    public static function bootHasLanguageProperties()
    {
        static::deleting(function($theModel){
            $theModel->translate()->delete();
        });
    }

    public function getTranslatedFromCollectionByLanguage(ILanguage $language,$translates)
    {


        $translation = new DummyTranslated();
        $languageId = $language->getId();
        foreach ($this->getLanguageProperties() as $property)
        {
            $readAttr = $translates->where("property",$property)->where("languageId",$languageId)->first();
            if($readAttr)
            {
                $translation->setAttribute($property,$readAttr->data);
            }
        }
        return $translation;


    }

}
