<?php


namespace Azizyus\LaravelLanguageHelper\App\Models;

use Illuminate\Http\Request;
use Illuminate\Support\Collection;


interface IMultipleLanguageObject
{

    public function getLanguageProperties() : array;

    public function translateEnum();

    public function translate();

    public function getTranslated(ILanguage $language);

    public function updateTranslation(ILanguage $language,$properties=[]);

    public function updateAllTranslations(Collection $languages,Request $request);

    public function getTranslatedFromCollectionByLanguage(ILanguage $language,$translates);


}
