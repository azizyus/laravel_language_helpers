<?php

namespace Azizyus\LaravelLanguageHelper\App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $title
 * @property string $shortTitle
 * @property boolean $isActive
 * @property boolean $isDefault
 * @property int $sort
 * @property string $created_at
 * @property string $updated_at
 * @property string $deleted_at
 * @property string $locale
 */
class Language extends Model implements ILanguage
{
    /**
     * @var array
     */
    protected $fillable = ['title', 'shortTitle', 'isActive', 'isDefault', 'sort', 'created_at', 'updated_at', 'deleted_at','locale'];

    /**
     * The storage format of the model's date columns.
     * 
     * @var string
     */
    protected $dateFormat = 'Y-m-d H:i:s';

    /**
     * The connection name for the model.
     * 
     * @var string
     */
    protected $connection = 'mysql';


    public function getId()
    {
        return $this->id;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function getShortTitle()
    {
        return $this->shortTitle;
    }

    public function getLocale()
    {
        return $this->locale;
    }


}
